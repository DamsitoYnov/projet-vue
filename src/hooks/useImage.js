export function useImage(comic) {
  const getImage = (image) => {
    return image.path + "." + image.extension;
  };
  const image =
    (comic.images && comic.images.length && getImage(comic.images[0])) ||
    getImage(comic.thumbnail);

  return {
    image
  };
}
