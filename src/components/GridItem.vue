<script setup>
import { toRefs } from "vue";
import { computed } from "vue";
import { useImage } from "../hooks/useImage";

const props = defineProps({
  element: {
    type: Object,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  width: {
    type: Number,
    default: 10,
  },
  height: {
    type: Number,
    default: 15,
  },
});

const { element, type } = toRefs(props);
const { image } = useImage(element.value);
const routeToGo = computed(() => {
  return { name: type.value, params: { id: element.value.id } };
});
</script>

<template>
  <div class="p-2 sm:w-1/3 md:w-1/3 lg:w-1/5">
    <div class="front">
      <router-link
          :to="routeToGo"
          class="flex flex-col items-center group gap-2">
        <img
            class="border-2 border-transparent group-hover:border-2 group-hover:border-gray-300 aspect-ratio-square object-fit"
            :src="image"
            :style="{ width: width + 'rem !important', height: height + 'rem !important' }"
            alt="image"
        />
      </router-link>
    </div>
    <div class="text-center text-white mt-1">
      <router-link v-if="type === 'character'" :to="'/characters/' + element.resourceURI.substring(element.resourceURI.indexOf('characters/') + 11)">{{ element.name }}</router-link>
    <span v-if="type === 'comic'">{{ element.title }}</span>
    </div>
    <slot></slot>
  </div>
</template>

<style scoped>
img {
  border-radius: 1rem;
}
</style>
