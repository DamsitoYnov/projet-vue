import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/index.css'
import infiniteScroll from 'vue-infinite-scroll'
createApp(App).use(router).use(infiniteScroll).mount('#app')
