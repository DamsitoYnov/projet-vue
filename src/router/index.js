import { createRouter, createWebHistory } from 'vue-router'
import Comic from "../pages/Comic.vue"
import Character from "../pages/Character.vue"
import Comics from "../pages/Comics.vue"
import Characters from "../pages/Characters.vue"
import Home from "../pages/Home.vue";
const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/comics',
    name: 'comics',
    component: Comics
  },
  {
    path: '/characters',
    name: 'characters',
    component: Characters
  },
  {
    path: '/comics/:id',
    name: 'comic',
    component: Comic
  },
  {
    path: '/characters/:id',
    name: 'character',
    component: Character
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
