module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    './node_modules/tw-elements/dist/js/**/*.js'
  ],
  plugins: [
    require('tw-elements/dist/plugin')
  ],
  theme: {
    extend: {
      colors: {
        'info-red': '#dd1818',
        'info-black': '#333333',
        'info-text-red': '#E34625',
      },
      fontFamily: {
        marvel: ["Marvel", "regular"],
      },
    },
  },
}
